﻿using System;
using System.IO;

namespace SimpleLog {
    public class Log {
        private string logFilePath;
        System.Text.UTF8Encoding utf8;
        DateTime dateTime;
        bool appendMode;

        public Log(string logName, string logFileDir = "./") {
            utf8 = new System.Text.UTF8Encoding(false);
            dateTime = DateTime.Now;
            string date = dateTime.ToString("yyyy-MM-dd");
            logFilePath = $"{logFileDir}{logName}.log";
        }

        public void Write(string message, string mode = "a") {
            if (mode == "a") {
                appendMode = true;
            } else {
                if (File.Exists(logFilePath)) { 
                    appendMode = true;                
                } else { 
                    appendMode = false;
                }
            }

            Console.WriteLine(message);

            using (var writer = new StreamWriter(logFilePath, appendMode, utf8)) {
                dateTime = DateTime.Now;
                writer.WriteLine($"{dateTime} {message}");
            }
        }
    }
}